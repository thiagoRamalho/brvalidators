package br.com.trama.validators.util;

public abstract class ConstantTest {

	public final static String INSC_ESTADUAL_SP = "110.042.490.114";

	public final static String INSC_ESTADUAL_SP_ONLY_NUMBERS = "110042490114";
	
	public final static String INSC_ESTADUAL_SP_INVALID_FDV = "110.042.480.114";
	
	public final static String INSC_ESTADUAL_SP_INVALID_SDV = "110.042.490.115";

	public final static String INSC_ESTADUAL_SP_INVALID_SIZE_MIN = "110.042.490.11";

	public final static String INSC_ESTADUAL_SP_INVALID_SIZE_MAX = "110.042.490.1145";

}
