package br.com.trama.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.validators.inscricaoestadual.InscricaoEstatdualSPStrategy;
import br.com.trama.validators.util.ConstantTest;
import br.com.trama.validators.util.ConstantsUtilities;
import br.com.trama.validators.util.RepeatedDigitGenerator;

public class AlgorithmValidatorTest {
	
	private AlgorithmValidator algorithmValidator;
	private RepeatedDigitGenerator rdg;

	@Before
	public void setUp(){
		algorithmValidator = new AlgorithmValidator(new InscricaoEstatdualSPStrategy());
		rdg = new RepeatedDigitGenerator();
	}

	@Test
	public void shouldBeOk(){
		assertTrue(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP));
	}

	
	@Test
	public void shouldBeErrorRepeatedDigit() {
	
		List<String> generate = rdg
		.generate(ConstantsUtilities.MIN_DIGIT, ConstantsUtilities.MAX_DIGIT, ConstantsUtilities.INSC_ESTADUAL_SP_SIZE);
		
		for (String repeatedDigit : generate) {
			assertFalse(algorithmValidator.isValid(repeatedDigit));
		}
	}
	
	@Test
	public void shouldBeErrorNullValue(){
		assertFalse(algorithmValidator.isValid(null));
	}

	@Test
	public void shouldBeErrorBlankValue(){
		assertFalse(algorithmValidator.isValid("   "));
	}


}
