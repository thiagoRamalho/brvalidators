package br.com.trama.validators.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.validators.util.RepeatedDigitGenerator;


public class RepeatedDigitGeneratorTest {

	private final int INI_RANGE = 0;
	private final int FINAL_RANGE = 9;
	private final int LENGTH = 14;
	private RepeatedDigitGenerator repeatedDigitGenerator;
	
	@Before
	public void setUp(){
		repeatedDigitGenerator = new RepeatedDigitGenerator();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldBeErrorFinalRangeInvalid() {
		
		repeatedDigitGenerator.generate(FINAL_RANGE, INI_RANGE, LENGTH);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldBeErrorInvalidLenghtOfRange() {
		
		repeatedDigitGenerator.generate(INI_RANGE, INI_RANGE, 0);
	}

	@Test
	public void shouldBeGenerateRanges(){
		
		List<String> generate = repeatedDigitGenerator.generate(INI_RANGE, FINAL_RANGE, LENGTH);
		
		assertEquals(1+FINAL_RANGE, generate.size());
		
		int idx = 0;
		
		for (String string : generate) {
			
			assertEquals(LENGTH, string.length());
			
			string = string.replaceAll(""+idx, "");
			
			assertTrue(string.trim().isEmpty());
			
			idx++;
		}
	}
}
