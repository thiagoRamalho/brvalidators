package br.com.trama.validators.inscricaoestadual;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.validators.AlgorithmValidator;
import br.com.trama.validators.util.ConstantTest;

public class InscricaoEstadualSPTest {
	
	private AlgorithmValidator algorithmValidator;
	
	@Before
	public void setUp(){
		algorithmValidator = new AlgorithmValidator(new InscricaoEstatdualSPStrategy());
	}

	
	@Test
	public void shouldBeOk(){
		assertTrue(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP));
	}
	
	@Test
	public void shouldBeOkOnlyNumbers(){
		assertTrue(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP_ONLY_NUMBERS));
	}
	
	@Test
	public void shouldBeErrorFirstDV(){
		assertFalse(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP_INVALID_FDV));
	}
	
	@Test
	public void shouldBeErrorSecondDV(){
		assertFalse(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP_INVALID_SDV));
	}
	
	@Test
	public void shouldBeErrorInvalidMin(){
		assertFalse(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP_INVALID_SIZE_MIN));
	}
	
	@Test
	public void shouldBeErrorInvalidMax(){
		assertFalse(algorithmValidator.isValid(ConstantTest.INSC_ESTADUAL_SP_INVALID_SIZE_MAX));
	}
}
