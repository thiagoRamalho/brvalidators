package br.com.trama.validators.util;

public abstract class ConstantsUtilities {

	private ConstantsUtilities(){}
	
	public static final String REGEX_DENIAL_NUMBER = "[[^0-9]]";
	public static final int SIZE_12 = 12;
	public static final int MIN_DIGIT = 0;
	public static final int MAX_DIGIT = 9;
	public static final int DIVIDER_11 = 11;
	
	public static final int INSC_ESTADUAL_SP_SIZE = 12;

}
