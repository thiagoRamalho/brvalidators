package br.com.trama.validators.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepeatedDigitGenerator {

	/**
	 * @param iniRange 
	 * @param finalRange
	 * @param lengthOfDigits
	 * @return
	 */
	public List<String> generate(int iniRange, int finalRange, int lengthOfDigits){
		
		if(iniRange > finalRange){
			throw new IllegalArgumentException("invalid finalRange");
		}
		
		if(lengthOfDigits < 1){
			throw new IllegalArgumentException("invalid lengthOfDigits");
		}
		
		List<String> result = new ArrayList<String>();
		
		for(int i = iniRange; i <= finalRange; i++){
			
			String[] repeated = new String[lengthOfDigits];
			
			Arrays.fill(repeated, ""+i);
			
			String finalDigits = Arrays.toString(repeated);
			
			result.add(finalDigits.replaceAll(ConstantsUtilities.REGEX_DENIAL_NUMBER, ""));
		}
	
		return result;
	}
}
