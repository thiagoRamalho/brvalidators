package br.com.trama.validators;

public interface AlgorithmStrategy {

	public String denialInvalidCharsRegex();

	public String discoverFirstDigitVerifier(String cleanValue);

	public String discoverSecondDigitVerifier(String cleanValue);

	public String buildFinalValue(String firstDV, String secondDV, String cleanValue);

	public int getSizeValue();
	
}
