package br.com.trama.validators.inscricaoestadual;

import br.com.trama.validators.AlgorithmStrategy;
import br.com.trama.validators.util.ConstantsUtilities;

/**
 * Referencia: http://pfe.fazenda.sp.gov.br/consist_ie.shtm
 * @author thiago
 *
 */
public class InscricaoEstatdualSPStrategy implements AlgorithmStrategy {
	
	private final int[] firstDigitMultiply  = {1,3,4,5,6,7,8,10};
	private final int[] secondDigitMultiply = {3,2,10,9,8,7,6,5,4,3,2};
	
	private final int FirstDigitVerifierPosition  = 9;
	private final int SecondDigitVerifierPosition = 12;
	
	
	
	public String denialInvalidCharsRegex() {
		return ConstantsUtilities.REGEX_DENIAL_NUMBER;
	}

	public String discoverFirstDigitVerifier(String cleanValue) {
		
		long total = multiplyTable(cleanValue, firstDigitMultiply);
		
		return div(total);
	}

	private long multiplyTable(String cleanValue, int[] table) {
		
		long total = 0;
		
		//segundo a regra devemos multiplicar os elementos pela
		//tabela definida para o digito e realizar a somatoria
		//dos resultados
		for (int i= 0; i< table.length; i++) {
			
			int digit = Integer.parseInt(""+cleanValue.charAt(i));
			int multiply = table[i];
			
			total+= digit * multiply;
		}
		
		return total;
	}

	private String div(long total) {
		
		//segundo a regra devemos obter o resto da divisao 
		long value = total % ConstantsUtilities.DIVIDER_11;
		
		String valueOf = String.valueOf(value);
		
		//conforme a regra devemos pegar o caracter mais a direita
		//do resultado da divisao
		return ""+valueOf.charAt(valueOf.length()-1);
	}

	public String discoverSecondDigitVerifier(String cleanValue) {
		
		long total = multiplyTable(cleanValue, secondDigitMultiply);
		
		return div(total);
	}

	public String buildFinalValue(String firstDV, String secondDV, String cleanValue) {
		
		StringBuilder builder = new StringBuilder(cleanValue);
		
		//sobrescreve os valores originais pelos calculados
		builder.replace(FirstDigitVerifierPosition-1, FirstDigitVerifierPosition, firstDV);
		builder.replace(SecondDigitVerifierPosition-1, SecondDigitVerifierPosition, secondDV);
		
		return builder.toString();
	}

	public int getSizeValue() {
		return ConstantsUtilities.INSC_ESTADUAL_SP_SIZE;
	}

}
