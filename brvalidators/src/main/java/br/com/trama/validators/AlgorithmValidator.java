package br.com.trama.validators;

import java.util.List;

import br.com.trama.validators.util.ConstantsUtilities;
import br.com.trama.validators.util.RepeatedDigitGenerator;

public class AlgorithmValidator {

	private RepeatedDigitGenerator digitGenerator;
	private AlgorithmStrategy algorithmStrategy;

	public AlgorithmValidator(AlgorithmStrategy algorithmStrategy) {
		this(algorithmStrategy, new RepeatedDigitGenerator());
	}

	public AlgorithmValidator(AlgorithmStrategy algorithmStrategy, RepeatedDigitGenerator digitGenerator) {
		super();
		this.algorithmStrategy = algorithmStrategy;
		this.digitGenerator = digitGenerator;
	}


	public boolean isValid(String value){

		boolean isValid = false;

		if(validValue(value)){
			
			//elimina qualquer elemento invalido no valor para validacao
			String cleanValue = value.replaceAll(algorithmStrategy.denialInvalidCharsRegex(), "");

			if(notRepeatedDigits(cleanValue) && cleanValue.length() == algorithmStrategy.getSizeValue()){
				
				//gera os digitos verificadores
				String firstDV  = algorithmStrategy.discoverFirstDigitVerifier(cleanValue);
				String secondDV = algorithmStrategy.discoverSecondDigitVerifier(cleanValue);
				String finalValue = algorithmStrategy.buildFinalValue(firstDV, secondDV, cleanValue);
				
				isValid = cleanValue.equals(finalValue);
			}
		}

		return isValid;
	}

	private boolean notRepeatedDigits(String cleanValue) {

		List<String> repeatedDigits = 

				this.digitGenerator.generate(
						ConstantsUtilities.MIN_DIGIT, 
						ConstantsUtilities.MAX_DIGIT, 
						this.algorithmStrategy.getSizeValue());


		return !repeatedDigits.contains(cleanValue);
	}

	private boolean validValue(String value) {

		return (value != null && (!value.trim().isEmpty()));
	}
}
